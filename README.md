# surveillance-self-defence-german

German translation of the Surveillance Self Defence guide written by the Electronic Frontier Foundation.

[Source](https://ssd.eff.org/en)


# How to Contribute? Wie kann ich mitmachen?

You are interested in privacy issues and are able to read and write German? Then, it would be nice to translate some portions of the original text. If you familiar with colaboration via codeberg, have a look at the [issues](https://codeberg.org/raketenlurch/surveillance-self-defence-german/issues). If you prefer not to subscribe to another platform you can contact the maintainer via [messenger](https://raketenlurch.codeberg.page/content/messenger.html) or [mastodon](https://chaos.social/@raketenlurch/105304826882888764).
